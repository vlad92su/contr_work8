@extends('layouts.base')
@section('content')

    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Applying for a book</h1>
            <hr>
            <form action="{{route('auth.store.application')}}" method="post">
                @csrf
                <input type="hidden" class="form-control" id="book_id" name="book_id" value="{{ $book->id }}">
                <div class="form-group">
                    <label for="library_card_number">Library card</label>
                    <input type="text" class="form-control" id="library_card_number"
                           @if(!is_null($library_card_number)) value="{{ $library_card_number }}" readonly
                           @endif name="library_card_number">
                </div>
                <div class="form-group">
                    <label for="author">Author</label>
                    <input type="text" class="form-control" id="author" name="author" value="{{ $book->author }}"
                           readonly>
                </div>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $book->title }}" readonly>
                </div>
                <div class="form-group">
                    <label for="return_date">Return Date</label>
                    <input type="date" class="form-control" id="return_date" name="return_date">
                </div>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary">Apply now</button>
                </div>
            </form>
        </div>
    </div>

@endsection
