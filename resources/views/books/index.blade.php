@extends('layouts.base')
@section('content')

    @foreach($genres as $genre)

        <h3><b>Genre: </b>{{$genre->name}}</h3>
        <div class="row">
            @foreach($genre->books as $book)
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img class="card-img-top" style="width: 100px; height: 100px"
                                 src="{{asset('/storage/' . $book->picture)}}" alt="{{ $book->picture}}">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title mt-2">Title: {{$book->title}}</h5>
                                <p class="card-text">Author: {{$book->author}}</p>
                                @if($book->application)
                                    @if($book->application->return_status === 'Ожидается' || $book->application->return_status == 'Просрочено')
                                        <p>Book taken before {{ $book->application->return_date }}</p>
                                    @elseif($book->application->return_status === 'Возвращено')
                                        <p class="card-text"><a href="{{ route('auth.create.application', $book) }}" class="btn btn-outline-secondary">Get the book</a></p>
                                    @endif
                                @else
                                    <p class="card-text"><a href="{{ route('auth.create.application',  $book) }}" class="btn btn-outline-secondary">Get the book</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    <div class="justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $genres->links() }}
        </div>
    </div>


@endsection
