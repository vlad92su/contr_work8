@extends('layouts.base')
@section('content')

    <h1>Hello {{ $user->last_name }}</h1>
    <p><b>Library card number: </b>{{ $user->library_card_number }}</p>

@endsection
