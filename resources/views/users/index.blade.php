@extends('layouts.base')

@section('content')

    <div class="row">
        <div class="col">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">Return date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($applications as $application)
                    <tr>
                        <div class="row">
                            <div class="col">
                                <td>{{$application->id}}</td>
                                <td>{{$application->book->title}}</td>
                                <td>{{$application->book->author}}</td>
                                <td>{{$application->return_date}}</td>
                                <td>{{$application->return_status}}</td>
                                @if($application->return_status == 'Ожидается' || $application->return_status == 'Просрочено')
                                    <td>
                                        <div class="row">
                                            <div class="col">
                                                <a href="{{ route('auth.return.book', ['application_id' => $application->id]) }}"
                                                   class="btn btn-primary">return book</a>
                                            </div>
                                        </div>
                                    </td>
                                @else
                                    <td></td>
                                @endif
                            </div>
                        </div>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
