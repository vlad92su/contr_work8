@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Log in</h1>
            <hr>
            <form action="{{route('auth.login')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="library_card_number">Library card</label>
                    <input type="text" class="form-control" id="library_card_number" name="library_card_number">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection
