<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [\App\Http\Controllers\BooksController::class, 'index'])->name('books.index');

Route::prefix('auth')->name('auth.')->group(function (){
    Route::get('register', [\App\Http\Controllers\UsersController::class, 'registerForm'])->name('register-form');
    Route::post('register', [\App\Http\Controllers\UsersController::class, 'store'])->name('register');

    Route::get('login', [\App\Http\Controllers\SessionsController::class, 'loginForm'])->name('login-form');
    Route::post('login', [\App\Http\Controllers\SessionsController::class, 'store'])->name('login');
    Route::delete('log-out', [\App\Http\Controllers\SessionsController::class, 'destroy'])->name('logout');

    Route::get('users/{user}', [\App\Http\Controllers\UsersController::class, 'show'])->name('show');
    Route::get('applications/{book}', [\App\Http\Controllers\ApplicationsController::class, 'create'])->name('create.application');
    Route::post('store', [\App\Http\Controllers\ApplicationsController::class, 'store'])->name('store.application');
    Route::get('return-book/{application_id}', [\App\Http\Controllers\ApplicationsController::class, 'returnBook'])->name('return.book');
    Route::get('users', [\App\Http\Controllers\UsersController::class, 'index'])->name('users.index');

});
