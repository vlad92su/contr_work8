<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'third_name' => $this->faker->lastName(),
            'address' => $this->faker->address(),
            'passport_number' => 'ID' . $this->faker->randomNumber(7),
            'password' => Hash::make('password'),
            'library_card_number' => $this->generateLibraryCartNum()
        ];
    }


    /**
     * @return string
     */
    function generateLibraryCartNum(): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $random_string = '';
        $length = 10;

        for ($i = 0; $i < $length; $i++) {
            $random_index = mt_rand(0, strlen($characters) - 1);
            $random_string .= $characters[$random_index];
        }

        return $random_string;
    }
}
