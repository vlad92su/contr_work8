<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => rand(1, 10),
            'book_id' => rand(1, 20),
            'return_date' => $this->faker->dateTimeBetween(Carbon::now()->subWeek(), Carbon::now()->addWeek())->format('d-m-Y'),
            'return_status' => $this->faker->randomElement(['Ожидается', 'Возвращено', 'Просрочено'])

        ];
    }
}
