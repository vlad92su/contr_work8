<?php

namespace Database\Seeders;

use App\Models\Genre;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $genres = [
            ['name' => 'Fantastic'],
            ['name' => 'Detective'],
            ['name' => 'Novel'],
            ['name' => 'Adventures'],
            ['name' => 'Scientific literature']
        ];

        foreach ($genres as $key => $value) {
            Genre::create($value);
        }
    }
}
