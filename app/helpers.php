<?php

/**
 * @return null
 */
function auth_user()
{
    if(session()->exists('user_id')){
        return \App\Models\User::find(session('user_id'));
    }
    return null;
}
