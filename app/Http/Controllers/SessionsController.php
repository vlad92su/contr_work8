<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionsController extends AuthController
{
    /**
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function loginForm(Request $request)
    {
        if($request->session()->exists('user_id')){
            return redirect()->route('books.index')->with('error', 'You are already log in');
        }
        return view('sessions.create');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $user = User::where('library_card_number', $request->get('library_card_number'))->first();

        if ($user) {
            if($this->auth($user, $request->get('password'))){
                $this->logIn($user);
                session()->put('library_card_number', $request->get('library_card_number'));
                return redirect()->route('books.index')->with('success', 'Hello ' . $user->name . '!');
            }
        }
        return redirect()->back()->with('error', 'Incorrect email or password!');
    }

    /**
     * @return RedirectResponse
     */
    public function destroy()
    {
        $this->logOut();
        return redirect()->route('auth.login-form')->with('success', 'You are success log out! Goodbye!');
    }

}
