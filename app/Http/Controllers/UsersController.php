<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\Application;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends AuthController
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        if(session()->exists('user_id')){
            $user_id = auth_user()->id;
            $applications = Application::where('user_id', $user_id)->get();
            return view('users.index', compact('applications'));
        }
        return view('sessions.create');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function registerForm(Request $request)
    {
        if($request->session()->exists('user_id')){
            return redirect()->route('books.index')->with('error', 'You are already register!');
        }
        return view('users.register');
    }


    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request)
    {
        $validated = $request->validated();
        $validated['password'] = Hash::make($validated['password']);
        $validated['library_card_number'] = $this->generateLibraryCartNum();
        session()->put('library_card_number', $validated['library_card_number']);

        $user = User::create($validated);
        $this->logIn($user);

        return redirect()->route('auth.show', compact('user'))->with('success', 'Registration successfully!');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }



    /**
     * @return string
     */
    function generateLibraryCartNum(): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $random_string = '';
        $length = 10;

        for ($i = 0; $i < $length; $i++) {
            $random_index = mt_rand(0, strlen($characters) - 1);
            $random_string .= $characters[$random_index];
        }

        return $random_string;
    }
}
