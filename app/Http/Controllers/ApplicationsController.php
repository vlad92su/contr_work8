<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Models\Application;
use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;

class ApplicationsController extends Controller
{

    /**
     * @param Book $book
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function create(Book $book)
    {
        if(session()->exists('library_card_number')) {
            $library_card_number = session()->get('library_card_number');
        } else {
            $library_card_number = null;
        }

        return view('applications.create', compact('book', 'library_card_number'));
    }


    /**
     * @param ApplicationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ApplicationRequest $request)
    {

        $user = User::where('library_card_number', $request->library_card_number)->first();

        $application = new Application();
        $application->book_id = $request->input('book_id');
        $application->user_id = $user->id;
        $application->return_date = $request->input('return_date');
        $application->return_status = 'Ожидается';
        $application->save();
        return redirect()->route('books.index')->with('success', "The book was successfully taken!!!");

    }


    /**
     * @param $applicationId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnBook($applicationId)
    {
        $application = Application::find($applicationId);
        $application->return_status = 'Возвращено';
        $application->return_date = now()->format('d-m-Y');
        $application->save();
        return redirect()->back()->with('success', 'The book was successfully returned!');

    }
}
